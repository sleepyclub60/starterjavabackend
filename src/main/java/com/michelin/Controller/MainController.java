package com.michelin.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {
   /*  This @CrossOrigin annotation enables cross-origin requests only for this specific method. 
	By default, its allows all origins, all headers, the HTTP methods specified in the @RequestMapping annotation and a maxAge of 30 minutes is used.
	You can customize this behavior by specifying the value of 
	one of the annotation attributes: origins, methods, allowedHeaders, exposedHeaders, allowCredentials or maxAge. 
	In this example, we only allow http://localhost:4500 to send cross-origin requests. */
	@RequestMapping("/api/message")
	@CrossOrigin(origins = "http://localhost:4500")
    public String message() {
        return "Response from Controller";
    }

}
